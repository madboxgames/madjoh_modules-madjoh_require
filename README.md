# madjoh_require #

v1.0.4

This module makes requireJS easier to use when your modules have their own dependencies.

## Getting Started ##

This module contains only one method : 

```js
MadJohRequire(require, dependencies);
```

This method is globally accessible and must not be overriden.

- ** require ** is the require parameter of the requireJS module.
- ** dependencies ** is a JSON object defining the dependencies of the module.

### Example ###

```js
define(function(require){
	if(typeof MadJohRequire === 'undefined'){
		console.log('MadJoh Require missing here !!');
		return;
	}
	var dependencies = {
		CustomEvents : '1.0.0',
		Controls : '1.0.0',
		PageTransition : '1.0.0',
		SweepEvent : '1.0.0'
	};
	var MadJoh = MadJohRequire(require, dependencies);
	var CustomEvents = MadJoh.CustomEvents;
	var Controls = MadJoh.Controls;
	var PageTransition = MadJoh.PageTransition;
	var SweepEvent = MadJoh.SweepEvent;

	// Now the dependencies are successfully loaded !
});
```