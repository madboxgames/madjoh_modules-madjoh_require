var MadJohRequire = {
	getList : function(dependencies, settings){
		var list = ['require'];

		if(Object.prototype.toString.call(dependencies) === '[object Array]'){
			for(var i=0; i<dependencies.length; i++) list.push('madjoh_modules/'+ dependencies[i]+ '/'+dependencies[i]);
			if(settings){
				for(var i=0; i<settings.length; i++) list.push('settings/'+ settings[i]+ '/'+settings[i]);
			}
			console.log('Deprecated require used with parameters : ', dependencies, settings);
		}else{
			for(var key in dependencies){
				if(key === 'madjoh_modules' || key === 'settings'){
					for(var i=0; i<dependencies[key].length; i++){
						list.push(key+'/'+dependencies[key][i] + '/' + dependencies[key][i]);
					}
				}else{
					for(var i=0; i<dependencies[key].length; i++){
						list.push(key+'/'+dependencies[key][i]);
					}
				}
					
			}
		}
			
		return list;
	},
	getCSS : function(deps){
		// CSS
		for(var i=0; i<deps.length; i++){
			if(!document.getElementById(deps[i]+'_css')){
				console.log('CSS not found for module '+deps[i]+'. You should add <link href="madjoh_modules/'+deps[i]+'/'+deps[i]+'.css"/> to your head Tag. Added automatically now.');
				var css = document.createElement('link');
					css.rel = 'stylesheet';
					css.type = 'text/css';
					css.id = deps[i]+'_css';
					var prefix = '';
					if (document.location.hostname !== 'localhost') prefix = 'http://madjoh.com/Games/JustDare/';
					css.href = prefix + 'madjoh_modules/'+deps[i]+'/'+deps[i]+'.css';
				document.getElementsByTagName('head')[0].appendChild(css);
			}
		}   
	}
};